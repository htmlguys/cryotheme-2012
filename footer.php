		
		<div class="clearfix"></div>
		<?php after_contents(); ?>
	</section><!-- end contents -->
	
	<footer id="subhead" role="contentinfo">
		<div class="wrapper">
			<?php before_footer(); ?>
			<?php _e( 'Copyright &copy; '.date("Y").'. All Rights Reserved - '.get_bloginfo( 'name' ), 'thg_framework' ); ?>
			<?php after_footer(); ?>
		</div>
	</footer><!-- #footer -->
	
</div><!-- main-wrapper -->

<?php wp_footer(); ?>
</body>
</html>