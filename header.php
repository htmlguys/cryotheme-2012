<!DOCTYPE html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!-- Consider adding a manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--><html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="description" content="<?php echo bloginfo('description'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Adding "maximum-scale=1" fixes the Mobile Safari auto-zoom bug: http://filamentgroup.com/examples/iosScaleBug/ -->

	<title><?php 
		// Print the <title> tag based on what is being viewed.
		global $page, $paged;
		wp_title( '|', true, 'right' );
		// Add the blog name.
		//bloginfo( 'name' );
		// Add the blog description for the home/front page.
		$site_description = get_bloginfo( 'description', 'display' );
		if ( $site_description && ( is_home() || is_front_page() ) )
			echo " | $site_description";
		// Add a page number if necessary:
		if ( $paged >= 2 || $page >= 2 )
			echo ' | ' . sprintf( __( 'Page %s', get_option('woo_shortname') ), max( $paged, $page ) );
	?></title>
	
	<?php 
		
		global $woo_options;
		
		$favicon = ( $woo_options['woohg_site_favicon'] == null ? $favicon = get_bloginfo( 'stylesheet_directory' )."/images/favicon.ico" : $woo_options['woohg_site_favicon'] ); 

	?>
 
    <link href="<?php echo $favicon; ?>" rel="shortcut icon" type="image/x-icon" />
	<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />
	
	<!-- Feeds and pingback //-->
    <link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url');?>" />
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	<?php
		/* We add some JavaScript to pages with the comment form
		 * to support sites with threaded comments (when in use).
		 */

		if ( is_singular() && get_option( 'thread_comments' ) )
			wp_enqueue_script( 'comment-reply' );

		/* Always have wp_head() just before the closing </head>
		 * tag of your theme, or you will break many plugins, which
		 * generally use this hook to add elements to <head> such
		 * as styles, scripts, and meta tags.
		 */
		wp_head(); 
	?>
</head>

<body <?php body_class(); ?>>

<div id="main-wrapper" class="hfeed site" role="main">
	
	<header id="masthead" class="branding" role="banner">
		
		<div class="wrapper">
			<?php before_header(); ?>
			<h1 class="site-logo">
				<a href="<?php echo home_url(); ?>">
					<?php if( $woo_options['woohg_site_logo'] <> null ): ?>
						<img src="<?php echo $woo_options['woohg_site_logo']; ?>" alt="<?php echo get_bloginfo( 'name' ); ?>" title="<?php echo get_bloginfo( 'description' ); ?>">
					<?php else: ?>
						<img src="<?php echo get_bloginfo( 'stylesheet_directory' ); ?>/images/logo.png" alt="<?php echo get_bloginfo( 'name' ); ?>" title="<?php echo get_bloginfo( 'description' ); ?>">
					<?php endif; ?>
				</a>
			</h1>

			<nav id="site-navigation" class="main-navigation" role="navigation">
				<?php wp_nav_menu( 
							array( 'theme_location' => 'main-navigation', 
								   'menu_class' => '', 
								   'container' => false,
								   'container_class' => ''
							) 
					  ); 
				?>
				<div class="clearfix"></div>
			</nav><!-- #site-navigation -->
			
			<?php after_header(); ?>
			<div class="clearfix"></div>
		</div>
	</header><!-- #masthead -->
	
	<section id="contents" class="wrapper">
		<?php before_contents(); ?>