<aside class="sidebar">

	<?php if( is_dynamic_sidebar( 'main-widget-area' ) ): ?>
		<?php dynamic_sidebar( 'main-widget-area' ); ?>
	<?php endif; ?>
	
</aside>