THINGS TO DO WHEN USING THE FRAMEWORK:

1. Update the Screenshot
2. Update STYLE.CSS accordingly
3. Update FUNCTION.PHP according to design specs. There are comments in that file that will guide you through
4. Update CUSTOM.CSS as this file is only used for preview. Write your own stylesheet as how the design require you to.
5. USE STYLES/MAIN.CSS as the main stylesheet and CUSTOM.CSS for overriding styles.
6. Update IMAGES/FAVICON.ICO.
7. Update IMAGES/LOGO.PNG with the appropriate project logo.
8. Update IMAGES/OPTION-LOGO.PNG with the appropriate project logo. This is used in the Theme Options Page
9. Update IMAGES/OPTION-ICON.PNG with the appropriate project logo. This is used in the Theme Options Tab Icon
10. Update IMAGES/SHORTCODE.JPG with the appropriate project logo. This is used in the Shortcode Button in the WordPress text Editor.
11. Update JS/REMOVE-WOOS.JS accordingly, specifically, update the theme name.
12. Update INCLUDES/THEME-OPTIONS.PHP accordingly, specifically, the THEME NAME
13. Update INCLUDES/THEME-SETUP.PHP accordingly, specifically, the THEME LOGO and THEME ICON