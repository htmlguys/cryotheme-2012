<?php

$shortname = "woohg";
$framework_icon = get_template_directory_uri() . '/images/option-icon.jpg';
$framework_logo = get_template_directory_uri() . '/images/option-logo.png';
update_option( 'framework_woo_backend_icon',$framework_icon);
update_option( 'framework_woo_backend_header_image',$framework_logo);
	
/*******************************
 Wordpress Menu Features
  -This is how you will enable the wordpress 3.0+ Menu System on your theme
********************************/

if ( function_exists( 'wp_nav_menu' ) ){
	if (function_exists('add_theme_support')) {
		add_theme_support('nav-menus');
		add_action( 'init', 'register_my_menus' );
		function register_my_menus() {
			register_nav_menus(
				array( 
					'main-navigation' => 'Main Navigation'
				)
			);
		}
	}
}

// REGISTER MORE JS & THIRD PARTY JS/PLUGIN
function register_more_js() {
	wp_register_script('mainjs', get_template_directory_uri() . "/js/main.js", null, null, true);
	// You may REGISTER more JS files here.
}
function enqueue_more_js() {
	wp_enqueue_script('mainjs');
	// You may ENQEUE more JS files here.
}

// REGISTER MORE CSS & THIRD PARTY CSS
function register_more_css() {
	// Add CSS
}

function enqueue_more_css() {
	// Add CSS
}

//##########################################//
//###### DO NOT CHANGE ANYTHING BELOW ######//
//##########################################//

function before_header(){ do_action( 'before_header' ); }
function after_header(){ do_action( 'after_header' ); }

function before_contents(){ do_action( 'before_contents' ); }
function after_contents(){ do_action( 'after_contents' ); }

function before_loop(){ do_action( 'before_loop' ); }
function after_loop(){ do_action( 'after_loop' ); }

function before_post(){ do_action( 'before_post' ); }
function after_post(){ do_action( 'after_post' ); }

function after_title(){ do_action( 'after_title' ); }
function after_article(){ do_action( 'after_article' ); }

function before_footer(){ do_action( 'before_footer' ); }
function after_footer(){ do_action( 'after_footer' ); }

function woohg_register_theme_js() {
	wp_deregister_script('jquery');
	wp_register_script('jquery', get_template_directory_uri()."/js/vendor/jquery-1.9.1.min.js", null, null, true);
	wp_register_script('hoverintent', get_template_directory_uri()."/js/vendor/jquery.hoverIntent.minified.js", null, null, true);
	wp_register_script('plugins', get_template_directory_uri()."/js/plugins.js", null, null, true);
	wp_register_script('modernizr', get_template_directory_uri()."/js/vendor/modernizr-2.6.2.min.js", null, null, false);
	wp_register_script('selectivizr', get_template_directory_uri()."/js/vendor/selectivizr-min.js", null, null, false);
}

function woohg_enqueue_theme_js() {
	wp_enqueue_script('modernizr');
	wp_enqueue_script('selectivizr');
	wp_enqueue_script('jquery');	
	wp_enqueue_script('hoverintent');
	wp_enqueue_script('plugins');	
}

function woohg_enque_admin_styles() {
	wp_register_style('adminstyleover', get_template_directory_uri() . "/styles/admin-style-override.css", null, null, false);
}

function woohg_enqueque_admin_styles() {
	wp_enqueue_style('adminstyleover');
}

function woohg_enque_admin_scripts() {
	wp_register_script('replacetext', get_template_directory_uri()."/js/vendor/jquery.ba-replacetext.min.js", null, null, true);
	wp_register_script('removewoogh', get_template_directory_uri()."/js/remove-woos.js", null, null, true);
}

function woohg_enqueque_admin_scripts() {
	wp_enqueue_script('replacetext');
	wp_enqueue_script('removewoogh');
}

if(!is_admin()) {
	add_action('init', 'woohg_register_theme_js'); 
	add_action('init', 'woohg_enqueue_theme_js'); 
	
	add_action('init', 'register_more_js'); 
	add_action('init', 'enqueue_more_js'); 
	
	add_action('init', 'register_more_css'); 
	add_action('init', 'enqueue_more_css'); 
}

if(is_admin()){
	add_action('init', 'woohg_enque_admin_styles'); 
	add_action('init', 'woohg_enqueque_admin_styles');
	add_action('init', 'woohg_enque_admin_scripts');
	add_action('init', 'woohg_enqueque_admin_scripts');
}